<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Produit; 
use App\Form\ProduitType; 
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class StoreController extends AbstractController
{
    /**
     * @Route("/store", name="store")
     */
    public function index(): Response
    {
        $repo=$this->getDoctrine()->getManager()->getRepository(Produit::class);
        $produits= $repo->findAll();
        return $this->render('store/index.html.twig', [
            'controller_name' => 'StoreController',
            'produits'=>$produits
        ]);
    }
/**
     * @Route("/", name="home")
     */

    public function home()
    {
return $this->render('store/home.html.twig' ,[ 
    'title'=>"bienvenue chez Doudi "
]);

    }
     /**
     * @Route("/brands", name="brands")
     */

    public function brands()
    {
        
return $this->render('store/brands.html.twig');

    }
    /**
     * @Route("/admin", name="admin")
     */

    public function admin():Response
    {
        $repo=$this->getDoctrine()->getManager()->getRepository(Produit::class);
        $produits= $repo->findAll();
return $this->render('store/admin.html.twig', [
    'controller_name' => 'StoreController',
    'produits'=>$produits
]);

    }
    /**
     * @Route("/store/{id}", name="product_show")
     */

    public function show($id)
    {
        $rep=$this->getDoctrine()->getRepository(Produit::class);
        $produit= $rep->find($id);
return $this->render('store/show.html.twig',['produit'=>$produit] );

    }
    /**
     * @Route("/creerclient", name="creer_client")
     */

    public function creer()
    { 

        
return $this->render('store/creer.html.twig' ,[ 
    'title'=>"Create your account and become our client", 'age'=> "you have 5 years old"
]);

    }
    /**
     * @Route("/addperfume", name="ajouterp")
     * @Route("/{id}/edit", name="modifierp")
    * @Route("/{id}/supprimer", name="supprimerp")
     */

    public function form(Produit $produit=null, Request $request)
    { 
        
     // $produit=new produit();
if (!$produit){
    $produit=new produit();
}


        $form=$this->createForm(ProduitType::class, $produit);
        

      $form->handleRequest($request);
      if($form->isSubmitted()&& $form->isValid())
      { 
          if(!$produit->getID()){
            $produit->setQuantite(1);
          }
        
        $em=$this->getDoctrine()->getManager();
        
        $em->persist($produit);
        $em->flush();

        
  return $this->redirectToRoute ('product_show',['id'=> $produit->getId()]);
    }
        
return $this->render('store/addp.html.twig' ,[ 
    'formProduct'=>$form->createView(), 'editMode'=>$produit->getId()!==null
]);

    }
     /**
     
    * @Route("/{id}/supprimer", name="supprimerp")
     */
    
    public function delete(Produit $produit)
    { 
       
        $em=$this->getDoctrine()->getManager();
        
        $em->remove($produit);
        $em->flush();

        return $this->redirectToRoute ('store',['id'=> $produit->getId()]);

    }
}
